.. Sphinx-nbblog documentation master file, created by
   sphinx-quickstart on Sat Sep 23 18:52:48 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sphinx-nbblog's documentation!
=========================================

This is the documentation and example for how to use Sphinx-nbblog.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   archive
   tags/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
