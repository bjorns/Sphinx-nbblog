Example
=======

.. nbblog::
   :date: 2023-10-01
   :abstract: An example of how things are done
   :tags: Sphinx, Blog, Example


.. tagpage:: Example


Feel free to look at the source in the link at the bottom.
