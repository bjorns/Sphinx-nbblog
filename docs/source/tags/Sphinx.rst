Tag: Sphinx
###########

.. tagpage:: Sphinx

.. toctree::
    :maxdepth: 1
    :hidden:

    ../about.rst
    ../example.rst
