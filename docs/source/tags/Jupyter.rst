Tag: Jupyter
############

.. tagpage:: Jupyter

.. toctree::
    :maxdepth: 1
    :hidden:

    ../2023/demo.ipynb
