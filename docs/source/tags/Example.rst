Tag: Example
############

.. tagpage:: Example

.. toctree::
    :maxdepth: 1
    :hidden:

    ../example.rst
