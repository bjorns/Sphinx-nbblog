About
=====

.. nbblog::
   :date: 2023-09-21
   :abstract: What this is all about
   :tags: Sphinx, Blog


This is a small Sphinx extension that provides :doc:`tags/index` and :doc:`archive` to documentation.
